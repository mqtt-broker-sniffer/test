# MQTT broker performance tests

To run the tests install the latest version of NodeJS, clone the repository, and run: 
```bash
# Install the dependencies
npm i
# To test the API
npm run test_API
# To test the MQTT publications
npm run text_mqtt
```