const { nanoid } = require("nanoid")
const fetch = require("node-fetch").default

const numberMessages = 100000
const interval = 100
const max = 10

const times = {}
const departs = {}
let i = 0

const clock = setInterval(() => {
    if (i < max){
        const id = nanoid()
        departs[id] = Date.now()
        fetch(`http://localhost:4040/published/629b7c5b3aad1e8a47f41f25?last=${numberMessages}`).then((_) => {
            times[id] = Date.now() - departs[id]
        })
    }

    i += 1
    if (Object.values(times).length === max) {
        const delays = Object.values(times)
        console.log(`The average latencies to fetch ${numberMessages} messages is ${delays.reduce((a, b) => a + b) / delays.length}ms`)
        clearInterval(clock)
    }
}, interval);