const mqtt = require("mqtt")
const { nanoid } = require("nanoid")

const max = 100
const interval = 20

const client = mqtt.connect('mqtt://localhost', {username: "629b7c5b3aad1e8a47f41f25"})

client.on('connect', () => {
    client.subscribe('test', (err) => {
        if (!err) {
            const departs = {}
            const times = {}
            let i = 0

            client.on('message', (topic, message) => {
                const now = Date.now()
                const id = message.toString()
                times[id] = now - departs[id]
            })

            const clock = setInterval(()=> {
                if (i < max) {
                    const id = nanoid()
                    departs[id] = Date.now()
                    client.publish('test', id)
                }
                
                i += 1
                if (Object.values(times).length === max) {
                    const finalTimes = Object.values(times)
                    console.log(`Broker latency with one MQTT message every ${interval}ms: ${finalTimes.reduce((a, b) => a + b) / finalTimes.length}ms`)
                    clearInterval(clock)
                    client.end()
                }
            }, interval)
        }
    })
})
